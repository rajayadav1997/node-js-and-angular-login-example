'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class LoginPath extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  LoginPath.init({
    name: DataTypes.STRING,
    pwd: DataTypes.STRING,
    type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'LoginPath',
  });
  return LoginPath;
};
