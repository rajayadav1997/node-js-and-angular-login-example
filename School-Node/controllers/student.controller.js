var models = require('../models');

exports.getQuestion  = async (req, res) => {    
    res.setHeader("Content-Type", "application/json");
    try{
        console.log(req.body);
        userId = null;
        const result = await models.LoginPath.findOne({where:{ name: req.body.user}},)
        .then(response =>  { 
            console.log(response);
            userId =    response;
        });
        if (userId == null){
            console.log("user not found");
            res.send({"error":true,"msg":"User Not Found"});
        }else{
                userId = userId.dataValues.id
                questions =null
                const getQuestion = await models.Question.findAll({where:{studentId:0} || {studentId:userId}},)
                    .then(response =>  { 
                        console.log(response);
                        questions =    response;
                    });
                if (questions == null){
                    console.log("user not found");
                    res.send({"error":true,"msg":"Test Not Found"});
                }else{
                    answerList = null
                    const getAnswerList = await models.StudentAns.findAll({where:{studentId:userId}},)
                    .then(response =>  { 
                        answerList =    response;
                    });
                    if(answerList.length == 0 ){
                        finalData =[]
                        for (qus in questions){
                            finalData.push({"id":questions[qus].dataValues.id, "testName":questions[qus].dataValues.testName,
                        "question":questions[qus].dataValues.question});
                        }    
                        res.send({"error":false,"msg":"Successfully fetch","data":finalData});
                    }else{
                        finalData =[]
                        for (qus in questions){
                            var idFound = false;
                            for(ansList in answerList){
                                if(answerList[ansList].dataValues.questionId == questions[qus].dataValues.id ){
                                    idFound = true;
                                        break;
                                }
                            }
                            if(!idFound){
                                finalData.push({"id":questions[qus].dataValues.id, "testName":questions[qus].dataValues.testName,
                            "question":questions[qus].dataValues.question});

                            }
                        }    
                        res.send({"error":false,"msg":"Successfully fetch","data":finalData});
                    }

                }
            }           
        
    }catch(e){
        console.log(e);
        res.send({"error":true,"msg":"Something Is Wrong"});
    }
}









exports.submitAnswer  = async (req, res) => {    
    res.setHeader("Content-Type", "application/json");
    try{
        console.log(req.body);
        userId = null;
        const result = await models.LoginPath.findOne({where:{ name: req.body.value.approver}},)
        .then(response =>  { 
            console.log(response);
            userId =    response;
        });
        if (userId == null){
            console.log("user not found");
            res.send({"error":true,"msg":"User Not Found"});
        }else{
                userId = userId.dataValues.id
                questions =null
                const getQuestion = await models.Question.findAll({where:{studentId:0} || {studentId:userId}},)
                .then(response =>  { 
                    console.log(response);
                    questions =    response;
                });
                if (questions == null){
                    console.log("user not found");
                    res.send({"error":true,"msg":"Test Not Found"});
                }else{
                    var userValue ={}
            for (ans in req.body.value.ans){
                user = {studentId: userId, questionId: parseInt(req.body.value.ans[ans].id),
                    answer: req.body.value.ans[ans].answer}
                     
                     const saveResult = await models.StudentAns.create(user).then(result => {
                        console.log('answer Save');
                    }).catch(error => {
                      console.log(error);
                    });

            }   

            res.send({"error":false,"msg":"Successfully Save"});
                }

            }

            

            
        
    }catch(e){
        console.log(e);
        res.send({"error":true,"msg":"Something Is Wrong"});
    }
}