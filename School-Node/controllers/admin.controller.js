var models = require('../models');

exports.getAnswer  = async (req, res) => {    
    res.setHeader("Content-Type", "application/json");
    try{
        console.log(req.body);
        userId = null;
        const result = await models.LoginPath.findOne({where:{ name: req.body.user}},)
        .then(response =>  { 
            console.log(response);
            userId =    response;
        });
        if (userId == null){
            console.log("user not found");
            res.send({"error":true,"msg":"User Not Found"});
        }else{
                userId = userId.dataValues.id
                answers =null
                const getQuestion = await models.StudentAns.findAll({where:{score:null}},)
                    .then(response =>  { 
                        console.log(response);
                        answers =    response;
                    });
                if (answers == null){
                    console.log("user not found");
                    res.send({"error":true,"msg":"All Report Done"});
                }else{
                    finalData = []
                        for (ans in answers){
                            questionResult=null;
                            const question = await models.Question.findOne({where:{id:answers[ans].dataValues.questionId}},)
                            .then(response =>  { 
                                console.log(response);
                                questionResult =    response;
                            });
                            if(questionResult == null){
                                finalData.push({"id":answers[ans].dataValues.id, "testName":"question id not found",
                                "question":"question id not found", "answer":answers[ans].dataValues.answer});
                            }else{
                                finalData.push({"id":answers[ans].dataValues.id, "testName":questionResult.dataValues.testName,
                                "question":questionResult.dataValues.question, "answer":answers[ans].dataValues.answer});
                            }
                            
                        }    
                        res.send({"error":false,"msg":"Successfully fetch","data":finalData});
                    

                }
            }           
        
    }catch(e){
        console.log(e);
        res.send({"error":true,"msg":"Something Is Wrong"});
    }
}

exports.submitScore  = async (req, res) => {    
    res.setHeader("Content-Type", "application/json");
    try{
        console.log(req.body);
            var userValue ={}
            for (ans in req.body.value.ans){
        user = {score: parseInt(req.body.value.ans[ans].score)}                
                const saveResult = await models.StudentAns.update(user,{where:{id:req.body.value.ans[ans].id}}).then(result => {
                console.log('answer Save');
            }).catch(error => {
                console.log(error);
            });

    }   

    res.send({"error":false,"msg":"Successfully Save"});
        

            
    }catch(e){
        console.log(e);
        res.send({"error":true,"msg":"Something Is Wrong"});
    }
}