var models = require('../models');

exports.findByUserNameAndPassword  = async (req, res) => {
    
    res.setHeader("Content-Type", "application/json");
    try{
        data = null;
        const result = await models.LoginPath.findOne({where:{ name: req.body.username, pwd: req.body.password}},)
        .then(response =>  { 
            console.log(response);
            data =    response;
        });
        if (data == null){
            console.log("data not found");
            res.send({"error":true});
        }else{
            console.log("data found");
            res.send({"id":data.dataValues.id,"name":data.dataValues.name,"type":data.dataValues.type,"error":false});
        }
    }catch(e){
        console.log(e);
        res.send({"error":true});
    }
}