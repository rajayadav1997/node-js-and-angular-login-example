module.exports = app => {

    var express = require('express');
    var router = express.Router();
    const student = require("../controllers/student.controller.js");

    router.post('/GetQuestion', student.getQuestion)  
    router.post('/SubmitAnswer', student.submitAnswer)  
    
    app.use('/student', router);
  };
  