module.exports = app => {

    var express = require('express');
    var router = express.Router();
    const admin = require("../controllers/admin.controller.js");

    router.post('/GetAnswer', admin.getAnswer)  
    router.post('/SubmitScore', admin.submitScore)  
    
    app.use('/admin', router);
  };
  