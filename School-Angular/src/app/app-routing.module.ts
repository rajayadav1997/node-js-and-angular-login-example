import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { StudentComponent } from './student/student.component';
import { TeacherComponent } from './teacher/teacher.component';
import { AdminComponent } from './admin/admin.component';
import { AlwaysAuthGuard } from './guard/guard.component';
import { AppComponent } from './app.component';
const routes: Routes = [
  {
      path: 'login',
      component: LoginComponent
  },
  {
    path: AppComponent.stuLink,
    component: StudentComponent,
    canActivate: [AlwaysAuthGuard],
    data: {role: 's'}
},
{
  path: AppComponent.teachLink,
  component: TeacherComponent,
  canActivate: [AlwaysAuthGuard],
  data: {role: 't'}
},
{
  path: AppComponent.admLink,
  component: AdminComponent,
  canActivate: [AlwaysAuthGuard],
  data: {role: 'a'}
},
  { path: '**', redirectTo: 'login' }
];
export const routing = RouterModule.forRoot(routes);


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
