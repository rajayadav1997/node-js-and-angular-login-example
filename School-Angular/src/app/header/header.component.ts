import { Component, OnInit } from '@angular/core';
import { RestServiceService } from '../rest-service/rest-service.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../Resources/css/animate.css',
  '../Resources/css/bootstrap.min.css',
  '../Resources/css/fontawsom-all.min.css','./header.component.css']
})
export class HeaderComponent implements OnInit {
  private menudisplay = "none";
  private userName = sessionStorage.getItem('currentUserSecond');
  
  constructor(private authenticationService : RestServiceService,
    private router: Router) { }


  ngOnInit() {
    if(!sessionStorage.getItem('currentUserSecond')){  
      this.logout();
    }
  }

  openmenu(){
    if(this.menudisplay == 'none'){
      this.menudisplay = 'block';
    }else{
      this.menudisplay = 'none';
    }
    
  }

  logout(){
    console.log('logout is working');
    this.authenticationService.logout();
    this.router.navigateByUrl('/login?response=Successfully logout');
  }
}
