import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Raja Yadav';
  public static userName = 'Login Error Plz Try Again';

  // client-portal link - 
  public static ip="http://localhost:3001";
  public static oauth= AppComponent.ip +"/login/GetCred";
  public static submitQuestion= AppComponent.ip +"/teacher/SubmitQuestion";
  public static getQuestion= AppComponent.ip +"/student/GetQuestion";

  public static getAnswer= AppComponent.ip +"/admin/GetAnswer";

  public static submitAnswer = AppComponent.ip +"/student/SubmitAnswer";

  public static submitScore = AppComponent.ip +"/admin/SubmitScore";

  public static stuLink = 'student';
  public static admLink = 'admin';
  public static teachLink = 'teacher';
  
}
