import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {AppComponent} from '../app.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }

  getQuestion(user){

    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.getQuestion,{"user":user},options)
        .pipe(map(user => {
          console.log('user pojo get data',user);            
            return user;
        }));
  }


  sendAnswer(value){
    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.submitAnswer,{value},options)
        .pipe(map(user => {
          console.log('user pojo get data',user);            
            return user;
        }));
  }

}
