import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AppComponent} from '../app.component';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class RestServiceService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
      // this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;

  }

  login(username: string, password: string) {
    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.oauth,{"username":username,"password":password},options)
        .pipe(map(user => {
          console.log('user pojo data',user);
            // login successful if there's a jwt token in the response
            if (!user['error']) {
              console.log('current user create');
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                sessionStorage.setItem('currentUser',  JSON.stringify(user));
                sessionStorage.setItem('currentUserSecond',username);
                // localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
            }
            return user;
        }));
}

logout() {
    console.log('user log out');
    sessionStorage.removeItem('currentUser');
    // localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
}

}
