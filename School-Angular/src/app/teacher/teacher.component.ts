import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup,  FormBuilder,  Validators, FormArray, FormControl } from '@angular/forms';
import { TeacherService } from './teacher.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  loading=false;
   angForm: FormGroup;
  constructor(private toastr: ToastrService,private router: Router,private fb: FormBuilder,private teacherService :TeacherService) { 
    this.angForm = this.fb.group({
      testName: ['', Validators.required ],
      user: ['0', Validators.required ],
      question: new FormArray([]),
      approver: [sessionStorage.getItem('currentUserSecond'), Validators.required ]
   });
   this.onAddQuestion();
  }

  get questionControl(){
    return (<FormArray>this.angForm.get('question')).controls;
  }

  onAddQuestion(){
    (<FormArray>this.angForm.get('question')).push(new FormControl('',[Validators.required ]));
  }
  
  ngOnInit() {
  }

  toast(){
    this.toastr.success('Success', 'All Fields Are Required');
  }

  submitQuestions(){
    console.log(this.angForm.value);
    if(this.angForm.value.testName == "" || this.angForm.value.user == ""){
     this.toast();
      return;
    }

    this.teacherService.sendQuestion(this.angForm.value)
    .pipe(first())
    .subscribe(
        data => {
          this.loading=false;
          console.error('my data..', data);
          if(! data.error){
            this.toastr.success('Success', data.msg);
            alert(data.msg);
            
            this.angForm = this.fb.group({
              testName: ['', Validators.required ],
              user: ['0', Validators.required ],
              question: new FormArray([]),
              approver: [sessionStorage.getItem('currentUserSecond'), Validators.required ]
           });
           this.onAddQuestion();
           
          }else{
            this.toastr.success('Error', data.msg);
          }
         
        },
        error => {
          this.loading=false;
          this.toastr.success('Success', 'error is comming we cannot submit');
        });

  }

  addQuestion(){
    this.onAddQuestion();
    this.toastr.success('Success', 'Successfully Add New Question');

  }

  deleteQuestion(){
    if((<FormArray>this.angForm.get('question')).length > 1){
      (<FormArray>this.angForm.get('question')).removeAt((<FormArray>this.angForm.get('question')).length -1);

    }else{
      this.toastr.success('Success', 'Minimum 1 Question is Required');
    }
  }
}