import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {AppComponent} from '../app.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }


  sendQuestion(value){
    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.submitQuestion,{value},options)
        .pipe(map(user => {
          console.log('user pojo data',user);
            
            return user;
        }));

  }

}
