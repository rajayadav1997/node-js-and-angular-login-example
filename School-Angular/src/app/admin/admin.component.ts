import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup,  FormBuilder,  Validators, FormArray, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { AdminService } from './admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  loading=false;
  testName="";
  submit="Test Not Found";
  angForm: FormGroup;

  constructor(private toastr: ToastrService,private router: Router,private fb: FormBuilder,
    private adminService:AdminService) { 
      this.angForm = this.fb.group({
        ans: this.fb.array([
        ]),
        approver: [sessionStorage.getItem('currentUserSecond'), Validators.required ]
     });
    }

    get questionControl(){
      return (<FormArray>this.angForm.get('ans')).controls;
    }

    ngOnInit() {
      this.getQuestion();
    }
  
    toast(){
      this.toastr.success('Success', 'All Fields Are Required');
    }

    
    ansSubmit(){
      console.log(this.angForm);
      
    this.adminService.sendScore(this.angForm.value)
    .pipe(first())
    .subscribe(
        data => {
          this.loading=false;
          if(! data.error){
            this.submit = "Successfully Submit, Please Reload the page, if you want to check new test.";
            this.angForm = this.fb.group({
              ans: this.fb.array([
              ]),
              approver: [sessionStorage.getItem('currentUserSecond'), Validators.required ]
           });           
          }else{
            this.toastr.success('Error', data.msg);
          }         
        },
        error => {
          this.loading=false;
          this.toastr.success('Success', 'error is comming we cannot submit');
        });


     
    }
  getQuestion(){        
    this.adminService.getAnswer(sessionStorage.getItem('currentUserSecond'))
    .pipe(first())
    .subscribe(
        data => {
          this.loading=false;
          console.error('my data..', data);
          if(! data.error){
                    for(var i=0; i<data.data.length; i++){
                      this.submit = "";
                      if(this.testName == ""){
                        this.testName= data.data[i].testName;
                      }
                      if( this.testName == data.data[i].testName ){
                        const arrayData = this.fb.group({
                                        id:this.fb.control(data.data[i].id,[Validators.required ]), 
                                        answer: new FormControl(data.data[i].answer,[Validators.required ]), 
                                        score: new FormControl('',[Validators.required ]), 
                                        question: this.fb.control(data.data[i].question,[Validators.required ]) });                       
                        (<FormArray>this.angForm.get('ans')).push(arrayData);
                      }
                    }
                       
          }else{
            this.toastr.success('Error', data.msg);
          }
         
        },
        error => {
          this.loading=false;
          this.toastr.success('Success', 'error is comming we cannot submit');
        });

  }  

}
