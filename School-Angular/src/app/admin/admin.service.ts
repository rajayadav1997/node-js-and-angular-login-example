import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {AppComponent} from '../app.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getAnswer(user){

    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.getAnswer,{"user":user},options)
        .pipe(map(user => {
          console.log('user pojo get data',user);            
            return user;
        }));
  }

  
  sendScore(value){
    const options = {headers: {'Content-Type':'application/json'}};
    return this.http.post<any>(AppComponent.submitScore,{value},options)
        .pipe(map(user => {
          console.log('user pojo get data',user);            
            return user;
        }));
  }

}
