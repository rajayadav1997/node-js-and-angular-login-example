import { Component, OnInit } from '@angular/core';
import {CanActivate,Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import { Injectable } from '@angular/core';
import { RestServiceService } from '../rest-service/rest-service.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-guard',
  templateUrl: './guard.component.html',
  styleUrls: ['./guard.component.css']
})
export class GuardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}


@Injectable({ providedIn: 'root' })
export class AlwaysAuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: RestServiceService
) { }

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> |boolean{
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser &&  route.data['role'] == currentUser['type']) {
        return true;
    }
    this.router.navigateByUrl('/login?response=need to login !');
    return false;
}

}
