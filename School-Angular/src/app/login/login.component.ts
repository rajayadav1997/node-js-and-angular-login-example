import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { RestServiceService } from '../rest-service/rest-service.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../Resources/css/animate.css',
  '../Resources/css/bootstrap.min.css',
  '../Resources/css/fontawsom-all.min.css',
  '../Resources/css/style.css']
})
export class LoginComponent implements OnInit {
  public loading = false;
  public response="";
  constructor( private router: Router,
    private route: ActivatedRoute,
    private authenticationService: RestServiceService) { }

  ngOnInit() {
    this.response = this.route.snapshot.queryParamMap.get('response');
    console.log('response ' ,this.response);
    this.authenticationService.logout();
  }
  onSubmit(value){
      
  this.loading=true;
  AppComponent.userName = value['username'];
console.log(value);
this.authenticationService.login(value['username'], value['password'])
            .pipe(first())
            .subscribe(
                data => {
                  this.loading=false;
                  console.error('my data..', data)
                  if(data['type'] == 's'){
                    this.router.navigateByUrl('/'+AppComponent.stuLink);
                  }else if(data['type'] == 'a'){
                    this.router.navigateByUrl('/'+AppComponent.admLink);
                  }else if(data['type'] == 't'){
                    this.router.navigateByUrl('/'+AppComponent.teachLink);
                  }else{
                    this.router.navigateByUrl('/login?response=need to login !');
                  }
                 
                },
                error => {
                  this.loading=false;
                  console.error('Error',error)
                  this.response =  "Wrong username and password";
                });

                this.response = this.route.snapshot.queryParamMap.get('response');
  }
  
}
